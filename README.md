## How to use
```
./new beg001/a.cc
./run beg001/a.cc
```

## Order
```
n < 10^6: O(n) or O(n log n)
n < 10^5: O(n log n)
n < 3000: O(n^2)
n < 300: O(n^3)
n < 20: O(2^n)
```