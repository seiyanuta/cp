#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

ull tori(ull n) {
    if (n <= 2)
        return 0;
    if (n == 3)
        return 1;

    ull r = 0;
    ull prevs[3] = {0, 0, 1};
    REP(i, n-3) {
//        cout << prevs[0] << ' '<<prevs[1] << ' ' << prevs[2] << ' ' << endl;
        prevs[i % 3] = r = (prevs[0]+prevs[1]+prevs[2]) % 10007;
    }

    return r;
}

int main() {
    ull n;
    cin >> n;
    cout << tori(n) % 10007 << endl;
    return 0;
}