#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    cin >> n;

    if (n % 3 == 0)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
    return 0;
}