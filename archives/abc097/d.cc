#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

template <typename T> class UnionFind {
    vector<T> parent_of;

public:
    UnionFind(T num_nodes): parent_of(num_nodes) {
        REP(i, num_nodes) {
            parent_of[i] = i;
        }
    }

    bool are_same_roots(T a, T b) {
        return root_of(a) == root_of(b);
    }

    T root_of(T a) {
        if (a == parent_of[a])
            return a;

        T root = root_of(parent_of[a]);
        parent_of[a] = root;
        return root;
    }

    void insert(T a, T parent) {
        merge(a, parent);
    }

    void merge(T a, T b) {
        parent_of[root_of(a)] = root_of(b);
    }
};

int main() {
    init();
    sll n, m;
    cin >> n >> m;
    vector<sll> ps(n);
    UnionFind<sll> swaps(n);
    REP(i, n) { cin >> ps[i]; ps[i]--; }
    REP(i, m) {
        sll x, y;
        cin >> x >> y;
        swaps.insert(x - 1, y - 1);
    }

    int ans = 0;
    REP(i, n) {
        if (swaps.are_same_roots(i, ps[i]))
            ans++;
    }

    cout << ans << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}