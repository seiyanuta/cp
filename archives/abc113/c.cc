#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

int prefs[100001];
int xs[100001];
int ids[100001];

int main() {
    init();
    sll n, m;
    cin >> n >> m;
    vector<pair<sll, sll> > cities(m);
    REP(i, m) {
        sll p, y;
        cin >> p >> y;
        cities[i] = make_pair(y, i);
        prefs[i] = p;
    }

    sort(cities.begin(), cities.end());

    for (auto &t: cities) {
        ids[t.second] = 1 + xs[prefs[t.second]]++;
    }

    REP(i, m) {
        cout << setfill('0') << setw(6) << prefs[i] << setfill('0') << setw(6) << ids[i] << endl;
    }

    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}