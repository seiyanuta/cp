#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    init();
    sll n;
    cin >> n;
    double t, a;
    cin >> t >> a;

    int d_i = -1;
    double d_min = MAX_OF(sll);
    REP(i, n) {
        sll h;
        cin >> h;
        double temp = t - h * 0.006;
        double d = abs(a - temp);
        if (d < d_min) {
            d_i = i;
            d_min = d;
        }
    }

    cout << d_i + 1 << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}