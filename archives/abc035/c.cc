#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    sll n, q;
    cin >> n >> q;
    vector<sll> peaces(n);

    REP(i, n) {
        peaces[i] = 0;
    }

    REP(i, q) {
        sll l, r;
        cin >> l >> r;
        l--;
        r--;
        for (sll j=l; j <= r; j++) {
            peaces[j]++;
        }
    }

    REP(i, n) {
        cout << peaces[i] % 2;
    }
    cout << endl;
    return 0;
}