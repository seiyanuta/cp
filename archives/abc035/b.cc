#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

static inline sll comp(sll x, sll y) {
    return abs(x) + abs(y);
}

int main() {
    string s;
    sll t;
    cin >> s;
    cin >> t;
    bool is_max = (t==1);

    sll x = 0, y = 0, q =0;
    for (char& c: s) {
        switch (c) {
            case 'L': x--; break;
            case 'R': x++; break;
            case 'U': y++; break;
            case 'D': y--; break;
            case '?': {
                q++;
                break;
            }
        }
    }

    if (is_max)
        if (x > 0)
            x+=q;
        else
            x-=q;
    else
        if (x > 0)
            x-=q;
        else
            x+=q;

    cout << abs(x) + abs(y) << endl;
    return 0;
}