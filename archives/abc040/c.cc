#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
#define DBG(expr) cout << #expr << " = " << expr << endl;
typedef signed long long ll;
typedef unsigned long long ull;

int main() {
    init();
    ll n;
    cin >> n;
    vector<ll> a(n);
    vector<ll> costs(n, MAX_OF(ll));
    REP(i, n) { cin >> a[i]; }
    costs[0] = 0;
    costs[1] = abs(a[1] - a[0]);
    REP_FROM(i, 2, n) {
        costs[i] = min(costs[i-1] + abs(a[i - 1] - a[i]), costs[i-2] + abs(a[i - 2] - a[i]));
    }
    cout << costs[n - 1] << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}