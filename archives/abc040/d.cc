#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;


template <typename T> class UnionFind {
    // Points to the parent if the value > 0  otherwise the # of nodes in
    // the cluster.
    vector<T> parent_of;

public:
    UnionFind(T num_nodes): parent_of(num_nodes) {
        REP(i, num_nodes) {
            parent_of[i] = -1;
        }
    }

    bool are_same_roots(T a, T b) {
        return root_of(a) == root_of(b);
    }

    T root_of(T a) {
        if (parent_of[a] < 0)
            return a;

        T root = root_of(parent_of[a]);
        parent_of[a] = root;
        return root;
    }

    void merge(T a, T b) {
        T ra = root_of(a);
        T rb = root_of(b);
        if (ra == rb)
            return;
        // Accumulate the # of nodes.
        parent_of[ra] += parent_of[rb];
        parent_of[rb] = ra;
    }

    size_t count(T a) {
        return -parent_of[root_of(a)];
    }
};


int main() {
    init();
    ll n, m;
    cin >> n >> m;
    vector<tuple<int, int, int> > roads(m);
    REP(i, m) {
        int a, b, y;
        cin >> a >> b >> y;
        roads[i] = make_tuple(y, a, b);
    }

    int q;
    cin >> q;
    vector<tuple<int, int, int> > qs(q);
    REP(i, q) {
        int v, w;
        cin >> v >> w;
        qs[i] = make_tuple(w, v, i);
    }

    SORT(roads);
    REVERSE(roads);
    SORT(qs);
    REVERSE(qs);
    vector<int> ans(q);
    UnionFind<int> uf(n+1);
    int j = 0;
    REP(i, m) {
        auto r = roads[i];
        auto y = get<0>(r);
        auto a = get<1>(r);
        auto b = get<2>(r);

next:
        if (j >= q) {
            break;
        }

        auto s = qs[j];
        auto w = get<0>(s);
        auto v = get<1>(s);
        auto q_i = get<2>(s);

//        printf("%d-%d (y=%d) w=%d (%d:%d)\n", a,b,y,w, q_i, j);
        if (y <= w) {
            ans[q_i] = uf.count(v);
            j++;
            goto next;
        }

        uf.merge(a, b);
    }

    REP_FROM(i, j, q) {
        auto s = qs[i];
        auto v = get<1>(s);
        auto q_i = get<2>(s);
        ans[q_i] = uf.count(v);
    }

    REP(i, q) {
        cout << ans[i] << endl;
    }

    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}