#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

int reachable[500][500];
int my_map[500][500];
int main() {
    init();
    sll h, w;
    cin >> h >> w;
    int start_x, start_y;
    int goal_x, goal_y;
    REP(i, h) {
        REP(j, w) {
            char sym;
            cin >> sym;
            if (sym == 's') {
                start_y = i;
                start_x = j;
            } else if (sym == 'g') {
                goal_y = i;
                goal_x = j;
            } else if (sym == '#') {
                my_map[i][j] = 1;
            }
        }
    }

    queue<pair<int, int> > q;
    q.push(make_pair(start_y, start_x));
    while (!q.empty()) {
        auto pos = q.front(); q.pop();
        auto y = pos.first;
        auto x = pos.second;
        if (y >= h || x >= w || y < 0 || x < 0 || reachable[y][x])
            continue;

        if (!my_map[y][x]) {
            reachable[y][x] = 1;
            q.push(make_pair(y - 1, x));
            q.push(make_pair(y + 1, x));
            q.push(make_pair(y, x + 1));
            q.push(make_pair(y, x - 1));
        }
    }

    cout << ((reachable[goal_y][goal_x]) ? "Yes" : "No") << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}