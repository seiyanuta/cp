#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

template <typename T> class UnionFind {
    vector<T> parent_of;

public:
    UnionFind(T num_nodes): parent_of(num_nodes) {
        REP(i, num_nodes) {
            parent_of[i] = i;
        }
    }

    bool are_same_roots(T a, T b) {
        return root_of(a) == root_of(b);
    }

    T root_of(T a) {
        if (a == parent_of[a])
            return a;

        T root = root_of(parent_of[a]);
        parent_of[a] = root;
        return root;
    }

    void insert(T a, T parent) {
        merge(a, parent);
    }

    void merge(T a, T b) {
        parent_of[root_of(a)] = root_of(b);
    }
};

int main() {
    init();
    sll n, q;
    cin >> n >> q;
    UnionFind<int> uf(n);
    REP(i, q) {
        sll p, a, b;
        cin >> p >> a >> b;
        if (p)
            cout << (uf.are_same_roots(a, b) ? "Yes" : "No") << endl;
        else
            uf.insert(a, b);
    }
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}