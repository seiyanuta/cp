#include <cstdio>
#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;

class Edge {
public:
    ll dst, cost;
    Edge(ll dst, ll cost) : dst(dst), cost(cost) {}
};

static const ll UNREACHABLE = MAX_OF(ll) / 10;
class Dijkstra {
    ll num_nodes;
    vector<vector<Edge> > edges;

public:
    Dijkstra(ll num_nodes) :
        num_nodes(num_nodes),
        edges(num_nodes, vector<Edge>()) {}

    void add_unidirect_edge(ll src, ll dst, ll cost) {
//        cout << "ADD: " << src << " -> " << dst << " (" << cost << ")" << endl;
        edges[src].emplace_back(dst, cost);
    }

    void add_bidirect_edge(ll node1, ll node2, ll cost) {
        add_unidirect_edge(node1, node2, cost);
        add_unidirect_edge(node2, node1, cost);
    }

    // Returns the minimal costs (or `UNREACHABLE`) to each nodes and the route.
    pair<vector<ll>, vector<ll> > search(ll start) {
        vector<ll> costs(num_nodes, UNREACHABLE);
        vector<ll> prev(num_nodes, -1);
        priority_queue<pair<ll, ll> > qs;
        costs[start] = 0;
        qs.push(make_pair(0, start));
        while (!qs.empty()) {
            auto q = qs.top();
            qs.pop();
            ll node = q.second;
            for (auto& e: edges[node]) {
                if (costs[e.dst] > costs[node] + e.cost) {
                    costs[e.dst] = costs[node] + e.cost;
                    prev[e.dst] = node;
                    qs.push(make_pair(-costs[e.dst], e.dst));
                }
            }
        }

        return make_pair(costs, prev);
    }
};

int main() {
    init();
    ll n, m, s, t;
    cin >> n >> m >> s >> t;
    Dijkstra snuke(n + 1);
    Dijkstra yen(n + 1);
    REP(i, m) {
        ll u, v, a, b;
        cin >> u >> v >> a >> b;
        yen.add_bidirect_edge(u, v, a);
        snuke.add_bidirect_edge(u, v, b);
    }

    auto costs_s = yen.search(s).first;
    auto costs_t = snuke.search(t).first;

    vector<ll> spent(n+1);
    REP_FROM(i, 1, n + 1) {
        spent[i] = costs_s[i] + costs_t[i];
    }

	for (int i = n - 1; i > 0; i--)
		spent[i] = min(spent[i], spent[i + 1]);

    REP_FROM(i, 1, n + 1) {
        ECHO(1000000000000000 - spent[i]);
    }

    return 0;
}

void init() {
#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif
    cin.tie(0);
    ios::sync_with_stdio(false);
}