#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    init();
    sll n, m, p;
    cin >> n >> m >> p;
    sll ans = 1;
    while (p > 0) {
        if ((p & 1) == 1)
            ans = (ans * n) % m;
        n = (n * n) % m;
        p = p >> 1;
    }

    cout << ans << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}