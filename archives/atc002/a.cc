#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
typedef signed long long sll;
typedef unsigned long long ull;

int m[51][51];
int steps[51][51];
int visited[51][51];
int main() {
    init();
    sll r, c, sy, sx, gy, gx;
    cin >> r >> c >> sy >> sx >> gy >> gx;
    REP(y, r) {
        REP(x, c) {
            char c;
            cin >> c;
            if (c == '#')
                m[y + 1][x + 1] = 1;
            steps[y + 1][x + 1] = MAX_OF(int) - 1;
        }
    }

    queue<pair<int, int> > q;
    q.push(make_pair(sy, sx));
    steps[sy][sx] = 0;
    while (!q.empty()) {
        auto p = q.front();
        auto y = p.first;
        auto x = p.second;
        q.pop();
        if (y <= 0 || x <= 0 || y > r || x > c || visited[y][x] || m[y][x])
            continue;

//        cout << y << ", " << x << endl;
        steps[y + 1][x] = min(steps[y + 1][x], steps[y][x] + 1);
        steps[y - 1][x] = min(steps[y - 1][x], steps[y][x] + 1);
        steps[y][x + 1] = min(steps[y][x + 1], steps[y][x] + 1);
        steps[y][x - 1] = min(steps[y][x - 1], steps[y][x] + 1);
        visited[y][x] = 1;

        q.push(make_pair(y + 1, x));
        q.push(make_pair(y - 1, x));
        q.push(make_pair(y, x + 1));
        q.push(make_pair(y, x - 1));
    }

    cout << steps[gy][gx] << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}