#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    init();
    sll n;
    cin >> n;
    vector<sll> as(n);
    vector<sll> bs(n);
    vector<sll> cs(n);
    REP(i, n) { cin >> as[i]; }
    REP(i, n) { cin >> bs[i]; }
    REP(i, n) { cin >> cs[i]; }
    SORT(as);
    SORT(bs);
    SORT(cs);
    sll sum = 0;
    REP(i, n) {
        auto a = lower_bound(ALL(as), bs[i]) - as.begin();
        auto c = n - (upper_bound(ALL(cs), bs[i]) - cs.begin());
        sum += a*c;
    }
   cout << sum << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}