#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    init();
    sll n, k;
    cin >> n >> k;
    vector<sll> t(n * n);
    vector<sll> a(n); REP(i, n) { cin >> a[i]; }
    vector<sll> b(n); REP(i, n) { cin >> b[i]; }
    REP(i, n) {
        REP(j, n) {
            t[i * n + j] = a[i] * b[j];
        }
    }
    SORT(t);
    cout << t[k - 1] << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}