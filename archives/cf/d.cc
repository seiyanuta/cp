#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
#define DBG(expr) cout << #expr << " = " << expr << endl;
typedef signed long long ll;
typedef unsigned long long ull;

int main() {
    init();
    ll n, m;
    cin >> n >> m;
    vector<ll> xs(m);
    vector<vector<ll> > dp(m+1, vector<ll>(n+1));
    REP(i, m) { cin >> xs[i]; }

    REP(i, m+1) {
        REP(j, n+1) {
            dp[i][j] = MAX_OF(ll);
        }
    }

    dp[0][0] = 0;
    REP(i,m) {
        REP(j, xs[i]) {
            REP_FROM(k, xs[i], n+1) {
                ll l = xs[i] - j;
                ll r = k - xs[i];
                ll d = min(r+l*2, l+r*2);
                dp[i+1][k] = min(dp[i+1][k], max(dp[i][j], d));
            }
        }
    }

    cout << dp[m][n] << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}