#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/
sll dp[10001];
int main() {
    init();
    sll n, m;
    cin >> n >> m;
    sll needs[] = {6,7,4,6,5,4,5,5,2};
    vector<sll> avail(m);
    REP(i, m) {
        cin >> avail[i];
    }
    sort(avail.begin(), avail.end());
    reverse(avail.begin(), avail.end());

    dp[0] = 0;
    for (int i=1; i < n; i++) {
        sll m = -1;
        REP(j, m) {
            if (i-avail[j] < 0)
                continue;
            m = max(m, dp[i-needs[j]]);
        }

        dp[i] = m + 1;
    }

    sll r = dp[n];
    while (r > 0) {
        REP(j, m) {
            int digit = avail[j];
            if (r == dp[r-needs[digit]] + 1) {
                cout << digit;
                r -= needs[digit];
                break;
            }
        }
    }

    cout << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}