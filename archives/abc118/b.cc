#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll n, m;
    cin >> n >> m;

    vector<sll> counts(m, 0);
    REP(i, n) {
        sll k;
        cin >> k;
        REP(j, k) {
            sll a;
            cin >> a;
            counts[a - 1]++;
        }
    }

    sll r = 0;
    REP(i, m) {
        if (counts[i] == n) {
            r++;
        }
    }

    cout << r << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}