#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll n;
    cin >> n;
    vector<sll> hp(n);
    REP(i, n) {
        cin >> hp[i];
    }

    sll lowest = -1;
    while(1) {
        sort(hp.begin(), hp.end());

        sll start = 0;
        for (; start < n; start++) {
            if (hp[start] > 0) {
                break;
            }
        }

        if (lowest == hp[start])
            break;

        lowest = hp[start];
        for (sll i = start + 1; i < n; i++) {
            hp[i] %= lowest;
        }
    }

    cout << lowest << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}