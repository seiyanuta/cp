#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll a, b;
    cin >> a >> b;

    if (a * b % 2 == 0)
        cout << "Even" << endl;
    else
        cout << "Odd" << endl;
    return 0;
}