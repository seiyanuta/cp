#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    cin >> n;
    vector<sll> ds;
    REP(i, n) {
        sll d;
        cin >> d;
        ds.push_back(d);
    }

    sort(ds.begin(), ds.end());
    sll prev = 0;
    sll r = 0;
    for (sll d: ds) {
        if (d == prev)
            continue;
        r++;
        prev = d;
    }

    cout << r << endl;

    return 0;
}