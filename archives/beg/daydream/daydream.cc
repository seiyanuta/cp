#include <iostream>
#include <tuple>
#include <vector>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int rec(const char *s) {
    if (s[0] == '\0')
        return 1;
    if (strncmp(s, "dreamer", 7) == 0 && rec(&s[7]))
            return 1;
    if (strncmp(s, "eraser", 6) == 0 && rec(&s[6]))
            return 1;
    if (strncmp(s, "dream", 5) == 0 && rec(&s[5]))
            return 1;
    if (strncmp(s, "erase", 5) == 0 && rec(&s[5]))
            return 1;

    return 0;
}

int main() {
    string s;
    cin >> s;

    if (rec(s.c_str()))
        cout << "YES" << endl;
    else
        cout << "NO" << endl;

    return 0;
}