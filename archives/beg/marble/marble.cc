#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    char a, b, c;
    cin >> a >> b >> c;

    int num = 0;
    num += a == '1';
    num += b == '1';
    num += c == '1';
    cout << num << endl;
    return 0;
}