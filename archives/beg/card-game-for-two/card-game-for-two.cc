#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    cin >> n;

    vector<sll> as;
    REP(i, n) {
        sll a;
        cin >> a;
        as.push_back(a);
    }

    sort(as.begin(), as.end(), greater<sll>());

    sll alice = 0;
    sll bob = 0;
    bool alice_turn = true;
    for (sll x: as) {
        if (alice_turn)
            alice += x;
        else
            bob += x;

        alice_turn = !alice_turn;
    }

    cout << alice - bob << endl;
    return 0;
}