#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n, a, b;
    cin >> n >> a >> b;

    sll res = 0;
    REP (i, n+1) {
        sll sum = 0;
        sll rem = i;
        sll d;
        do {
            d = rem % 10;
            sum += d;
            rem /= 10;
        } while (rem > 0);

        if (a <= sum && sum <= b) {
            res += i;
        }
    }

    cout << res << endl;
    return 0;
}