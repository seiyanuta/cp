#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    cin >> n;
    sll prev_t = 0, prev_x = 0, prev_y = 0;
    REP(i, n) {
        sll t, x, y;
        cin >> t >> x >> y;
        sll moves = t - prev_t;

        sll min_move = abs(x - prev_x) + abs(y - prev_y);
        if (moves < min_move) {
            goto bad;
        }

        if (moves % 2 != min_move %2) {
            goto bad;
        }

        prev_x = x;
        prev_y = y;
        prev_t = t;
    }

    cout << "Yes" << endl;
    return 0;

bad:
    cout << "No" << endl;
    return 0;
}