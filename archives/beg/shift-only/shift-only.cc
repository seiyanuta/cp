#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    vector<sll> as;
    cin >> n;
    sll res = numeric_limits<sll>::max();
    REP(i, n) {
        sll a;
        cin >> a;
        sll shifts = 0;
        while ((a & 1) == 0) {
            shifts++;
            a >>= 1;
        }
        res = min(res, shifts);
    }

    cout << res << endl;
    return 0;
}