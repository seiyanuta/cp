#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n, y;
    cin >> n >> y;

    REP(i, n+1) {
        REP(j, n-i+1) {
            REP(k, n-(i+j)+1) {
                if (i+j+k==n && y == 10000*i+5000*j+1000*k) {
                    cout << i << ' ' << j << ' ' << k << endl;
                    return 0;
                }
            }
        }
    }

    cout << -1 << ' ' << -1 << ' ' << -1 << endl;
    return 0;
}