#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll a, b, c;
    string s;
    cin >> a;
    cin >> b >> c;
    cin >> s;

    cout << a + b + c << ' ' << s << endl;
    return 0;
}