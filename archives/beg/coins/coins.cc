#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(int var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll a, b, c, x;
    cin >> a;
    cin >> b;
    cin >> c;
    cin >> x;

    int res = 0;
    REP(i, a) {
        REP(j, b) {
            REP(k, c) {
                if (500*a+100*b+50*c == x)
                    res++;
            }
        }
    }

    cout << x <<endl;
    return 0;
}