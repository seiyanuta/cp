#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;

class Edge {
public:
    ll src, dst, cap, rev;
    Edge(ll src, ll dst, ll cap, ll rev) : src(src), dst(dst), cap(cap), rev(rev) {}
};

class Dinic {
    ll num_nodes;
    vector<vector<Edge> > edges;
    vector<ll> distances;
    vector<ll> visited;

public:
    Dinic(ll num_nodes)
        : num_nodes(num_nodes)
        , edges(num_nodes)
        , distances(num_nodes, -1)
        , visited(num_nodes)
        {}

    void add_edge(ll src, ll dst, ll cap) {
        edges[src].emplace_back(src, dst, cap, edges[dst].size());
        edges[dst].emplace_back(dst, src, 0, edges[src].size() - 1);
    }

    ll max_flow(ll start, ll goal) {
        ll ans = 0;
        while (true) {
            bfs(start);
            if (distances[goal] < 0) {
                return ans;
            }

            visited.assign(num_nodes, 0);
            ll f;
            while ((f = dfs(start, goal, MAX_OF(ll))) > 0)
                ans += f;
        }
    }

private:
    // Computes the distances between `start` and each nodes whose
    // current capacity is not 0.
    void bfs(ll start) {
        distances.assign(num_nodes, -1);
        distances[start] = 0;
        queue<ll> qs;
        qs.push(start);
        while (!qs.empty()) {
            ll v = qs.front(); qs.pop();
            for (Edge& e: edges[v]) {
                if (e.cap > 0 && distances[e.dst] < 0) {
                    distances[e.dst] = distances[v] + 1;
                    qs.push(e.dst);
                }
            }
        }
    }

    ll dfs(ll src, ll dst, ll flow) {
        if (src == dst)
            return flow;

        for (ll& i = visited[src]; i < (ll) edges[src].size(); i++) {
            Edge& e = edges[src][i];
            if (e.cap > 0 && distances[src] < distances[e.dst]) {
                ll f = dfs(e.dst, dst, min(flow, e.cap));
                if (f > 0) {
                    e.cap -= f;
                    edges[e.dst][e.rev].cap += f;
                    return f;
                }
            }
        }

        return 0;
    }
};


void solve() {
    ll n, m, p, g;
    cin >> n >> m >> p >> g;
    Dinic graph(m + 1);
    REP(i, g) {
        ll provider;
        cin >> provider;
        graph.add_edge(m /* from stone */, provider, MAX_OF(ll));
    }
    REP(i, n) {
        ll src, dst, cap;
        cin >> src >> dst >> cap;
        graph.add_edge(src, dst, cap);
    }

    ll max_flow = graph.max_flow(m, 0);
    if (max_flow > p)
        ECHO("Yes")
    else
        ECHO("No")
}

int main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif

    solve();
    return 0;
}