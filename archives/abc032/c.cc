#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;

void solve() {
    ll n, k;
    cin >> n >> k;
    vector<ll> ss(n);
    REP(i, n) {
        cin >> ss[i];
        if (!ss[i]) {
            ECHO(n)
            return;
        }
    }

    ll l = 0, r =0;
    ll ans = 0;
    ll acc = 1;
    while (l < n) {
//        printf("[%lld, %lld], acc=%lld, ss[r]=%lld\n", l, r, acc, ss[r]);
        while (acc * ss[r] <= k) {
            acc *= ss[r];
            ans = max(ans, r - l + 1);
            if (r == n - 1) {
                break;
            }

            r++;
        }

        acc /= ss[l];
        if (!acc)
            acc = 1;
        l++;
        if (l > r)
            r = l;
    }

    ECHO(ans)
}

int main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif

    solve();
    return 0;
}