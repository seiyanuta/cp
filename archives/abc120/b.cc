#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll a,b,k;
    cin >> a >> b >> k;

    for (sll i = min(a, b); i >= 1; i--) {
        if (a % i != 0 || b % i != 0)
            continue;

        k--;
        if (!k) {
            cout << i << endl;
            break;
        }
    }
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}