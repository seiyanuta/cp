#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    string cubes;
    cin >> cubes;

    sll zeros = 0;
    sll ones = 0;
    for (size_t i = 0; i < cubes.length(); i++) {
        if (cubes[i] == '0')
            zeros++;
        else
            ones++;
    }

    cout << 2*min(zeros, ones) << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}