#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;

void solve() {
    ll n;
    cin >> n;
    string s;
    cin >> s;
    vector<ll> chars(26, 1);
    for (char& c: s) {
        chars[c - 'a']++;
    }

    ll ans = 1;
    REP(i, 26) {
        ans = (ans * chars[i]) % 1000000007;
    }

    ans -= 1;
    ECHO(ans);
}

int main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif

    solve();
    return 0;
}