#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll n;
    cin >> n;

    double sum = 0;
    REP(i, n) {
        double x;
        string type;
        cin >> x >> type;

        if (type == "JPY")
            sum += x;
        else
            sum += x * 380000.0;
    }

    cout << sum << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}