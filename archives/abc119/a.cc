#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll y,m,d;
    char s;
    cin >> y>>s>>m>>s>>d;
    if (y < 2019 || (y == 2019 && m < 4) || (y == 2019 && m == 4 && d <= 30))
    cout << "Heisei" << endl;
    else
    cout << "TBD" << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}