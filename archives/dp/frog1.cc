#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

int main() {
    sll n;
    cin >> n;
    vector<sll> hs(n);
    vector<sll> dp(n);

    REP(i, n) {
        cin >> hs[i];
    }

    REP(i, n) {
        if (i == 0) {
            dp[i] = 0;
        } else if (i == 1) {
            dp[i] = abs(hs[1] - hs[0]);
        } else {
            dp[i] = min(
                abs(hs[i] - hs[i - 1]) + dp[i - 1],
                abs(hs[i] - hs[i - 2]) + dp[i - 2]
            );
        }
    }

    cout << dp[n - 1] << endl;
    return 0;
}