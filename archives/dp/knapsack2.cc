#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

sll dp[101][1001];

int main() {
    init();
    sll n, total_cap;
    cin >> n >> total_cap;

    REP(c, total_cap) {
        dp[0][c] = 0;
    }

    vector<sll> ws(n);
    vector<sll> vs(n);
    for (sll i = 0; i < n; i++) {
        cin >> ws[i] >> vs[i];
    }

    for (sll i = 0; i < n; i++) {
        for (sll v_max = 0; v_max <= 1000; v_max++) {
            dp[i+1][v_max] = min(dp[i][v_max - vs[i]] + ws[i], dp[i][v_max]);
            dp[i+1][v_max] = min(dp[i+1][v_max], dp[i][v_max]);
        }
    }

    cout << dp[n][total_cap] << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}