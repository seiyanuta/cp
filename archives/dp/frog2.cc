#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

/*
numeric_limits<sll>::max()
*/

int main() {
    sll n, k;
    cin >> n >> k;
    vector<sll> hs(n);
    vector<sll> dp(n);

    REP(i, n) {
        cin >> hs[i];
    }

    REP(i, n) {
        if (i == 0) {
            dp[i] = 0;
        } else if (i == 1) {
            dp[i] = abs(hs[1] - hs[0]);
        } else {
            dp[i] = numeric_limits<sll>::max();
            for (sll j = 1; j <= i && j <= k; j++) {
                dp[i] = min(dp[i], abs(hs[i] - hs[i - j]) + dp[i - j]);
            }
        }
    }

    cout << dp[n - 1] << endl;
    return 0;
}