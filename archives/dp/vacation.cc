#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;

sll dp[100000][3];
sll vs[100000][3];

int main() {
    sll n;
    cin >> n;

    REP(i, n) {  cin >> vs[i][0] >> vs[i][1] >> vs[i][2]; }
    REP(i, n) {
        if (i == 0) {
            dp[0][0] = vs[0][0];
            dp[0][1] = vs[0][1];
            dp[0][2] = vs[0][2];
        } else {
            REP(j, 3) {
                dp[i][j] = numeric_limits<sll>::min();
                REP(k, 3) {
                    if (j == k)
                        continue;

                    dp[i][j] = max(dp[i][j], dp[i - 1][k] + vs[i][j]);
                }
            }
        }
    }

    sll r = 0;
    REP(i, 3) {
        r = max(r, dp[n - 1][i]);
    }
    cout <<r << endl;
    return 0;
}