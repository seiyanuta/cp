#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();

    sll n, m;
    cin >> n >> m;
    vector<pair<sll, sll>> shops(n);
    REP(i, n) {
        sll a, b;
        cin >> a >> b;
        shops[i] = make_pair(a, b);
    }

    sort(shops.begin(), shops.end());

    sll cost = 0;
    REP(i, n) {
        if (!m) {
            break;
        }

        sll bought = min(m, shops[i].second);
        cost += bought * shops[i].first;
        m -= bought;
    }

    cout << cost << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}