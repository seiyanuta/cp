#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

sll bs[20];
int main() {
    init();
    sll n, m, c;
    cin >> n >> m >> c;
    REP(i, m) { cin >> bs[i]; }
    sll res = 0;
    REP(i, n) {
        sll sum = c;
        REP(i, m) {
            sll a;
            cin >> a;
            sum += a * bs[i];
        }

        if (sum > 0) {
            res++;
        }
    }

    cout << res << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}