#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long sll;
typedef unsigned long long ull;

sll hs[100000];
sll ss[100000];
vector<pair<sll, sll> > rs(100000);

int main() {
    init();
    sll n;
    cin >> n;

    REP(i, n) {
        cin >> hs[i] >> ss[i];
    }

    sll l = 0;
    sll r = 1e18;
    sll pena;
    while (r - l > 0) {
        sll mid = (l + r) / 2;
        bool ok = false;
        REP(i, n) {
            sll r = (mid - hs[i]) / ss[i];
            rs[i] = make_pair(r, i);
            if (r < 0) {
                goto err;
            }
        }

        SORT(rs);
        pena = -1;
        REP(i, n) {
            sll idx = rs[i].second;
            pena = max(pena, hs[idx] + i * ss[idx]);
            if (i < rs[i].first) {
                goto err;
            }
        }

        ok = true;
    err:
        if (ok) {
            l = mid;
        } else {
            r = mid;
        }
    }

    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}