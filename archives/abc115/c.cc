#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll n, k;
    cin >> n >> k;
    vector<int> hs(n);
    REP(i, n) {
        cin >> hs[i];
    }

    sort(hs.begin(), hs.end());
    int min_d = numeric_limits<int>::max();
    for (sll i = 0; i <= n - k; i++) {
        min_d = min(min_d, hs[i + k - 1] - hs[i]);
    }

    cout << min_d << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}