#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int main() {
    init();
    sll n;
    cin >> n;

    sll highest = 0;
    sll sum = 0;
    REP(i, n) {
        sll p;
        cin >> p;
        sum += p;
        highest = max(highest, p);
    }

    cout << sum - highest + (highest / 2) << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}