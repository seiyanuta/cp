#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << expr << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;

int fst[100001];
int snd[100001];

void solve() {
    ll n;
    cin >> n;
    ll f, s;
    REP(i, n) {
        if (i % 2 == 0) {
            cin >> f;
            fst[f]++;
        } else {
            cin >> s;
            snd[s]++;
        }
    }

    sort(begin(fst), end(fst));
    sort(begin(snd), end(snd));
    ll n1 = 0, n2 = 0;
    REP(i, 100000) { n1 += fst[i]; }
    REP(i, 100000) { n2 += snd[i]; }
    if (n1 == 0 && n2 == 0 && f == s)
        ECHO(min())
    else
        ECHO(n1 + n2)
}

int main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif

    solve();
    return 0;
}