#include <iostream>
#include <iomanip>
#include <tuple>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#ifdef DEBUG_BUILD
#define DBG(expr) cout << #expr << " = " << (expr) << endl;
#endif
#define ECHO(expr) cout << expr << endl;
#define REP(var, num) for(ll var = 0; var < num; var++)
#define REP_FROM(var, from, num) for(ll var = from; var < num; var++)
#define MAX_OF(type) numeric_limits<type>::max()
#define MIN_OF(type) numeric_limits<type>::min()
#define SORT(vec) sort(vec.begin(), vec.end())
#define REVERSE(vec) reverse(vec.begin(), vec.end())
#define ALL(vec) vec.begin(), vec.end()
typedef signed long long ll;
typedef unsigned long long ull;
class Edge {
public:
    ll dst, cost;
    Edge(ll dst, ll cost) : dst(dst), cost(cost) {}
};

static const ll UNREACHABLE = MAX_OF(ll) / 10;
class Dijkstra {
    ll num_nodes;
    vector<vector<Edge> > edges;

public:
    Dijkstra(ll num_nodes) :
        num_nodes(num_nodes),
        edges(num_nodes, vector<Edge>()) {}

    void add_unidirect_edge(ll src, ll dst, ll cost) {
        edges[src].emplace_back(dst, cost);
    }

    void add_bidirect_edge(ll node1, ll node2, ll cost) {
        add_unidirect_edge(node1, node2, cost);
        add_unidirect_edge(node2, node1, cost);
    }

    // Returns the minimal costs (or `UNREACHABLE`) to each nodes and the route.
    pair<vector<ll>, vector<ll> > search(ll start) {
        vector<ll> costs(num_nodes, UNREACHABLE);
        vector<ll> prev(num_nodes, -1);
        priority_queue<pair<ll, ll> > qs;
        costs[start] = 0;
        qs.push(make_pair(0, start));
        while (!qs.empty()) {
            auto q = qs.top();
            qs.pop();
            ll node = q.second;
            for (auto& e: edges[node]) {
                if (costs[e.dst] > costs[node] + e.cost) {
                    costs[e.dst] = costs[node] + e.cost;
                    prev[e.dst] = node;
                    qs.push(make_pair(-costs[e.dst], e.dst));
                }
            }
        }

        return make_pair(costs, prev);
    }
};

template<class ForwardIterator, class T>
ll range_binary_search(ForwardIterator first, ForwardIterator last, const T& needle) {
    ll left = 0, right = last - first;
    while (right - left > 1) {
        ll mid = (left + right) / 2;
        // printf(">>> (%lld, %lld) needle=%lld, mid=%lld (%lld)\n", left, right, needle, mid, first[mid]);
        if (needle < first[mid])
            right = mid;
        else
            left = mid;
    }

    return left;
}

void solve() {
    ll n, m, k, s, g;
    cin >> n >> m >> k >> s >> g;
    Dijkstra dk(n);
    REP(i, m) {
        ll a, b, d;
        cin >> a >> b >> d;
        dk.add_bidirect_edge(a, b, d);
    }

    vector<ll> distances(k);
    vector<ll> fees(k);
    REP(i, k) {
        ll x, f;
        cin >> x >> f;
        distances[i] = x;
        fees[i] = f;
    }

    ll ans = MAX_OF(ll);
    auto cs1 = dk.search(s).first;
    auto cs2 = dk.search(g).first;
    REP(i, n) {
        auto c1 = cs1[i];
        auto c2 = cs2[i];
//        printf("%lld -> %lld -> %lld distance=(%lld, %lld)\n", s, i, g, c1, c2);
        if (c1 == UNREACHABLE || c2 == UNREACHABLE)
            continue;
        auto f1 = range_binary_search(distances.begin(), distances.end(), c1);
        auto f2 = range_binary_search(distances.begin(), distances.end(), c2);
        ans = min(ans, ((s == i) ? 0 : fees[f1]) + ((i == g) ? 0 : fees[f2]));
//        printf("%lld -> %lld -> %lld, fee = %lld (%lld, %lld)\n", s, i, g, fees[f1] + fees[f2], fees[f1], fees[f2]);
    }

    ECHO(ans)
}

int main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

#ifdef INPUT_FILE
    if (!freopen(INPUT_FILE, "r", stdin)) {
        perror("freopen");
        exit(1);
    }
#endif

    solve();
    return 0;
}