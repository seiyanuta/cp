#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, num) for(sll var = 0; var < num; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

bool memo[1000000];
int main() {
    init();
    sll s;
    cin >> s;

    REP(i, 1000000) { memo[i] = false; }
    sll a = s;
    sll i = 2;
    memo[s] = true;
    for (;; i++) {
        a = (a % 2 == 0) ? a/2 : 3 *a + 1;
        if (memo[a])
            break;
        memo[a] = true;
    }

    cout << i << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}