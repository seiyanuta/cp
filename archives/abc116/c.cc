#include <iostream>
#include <tuple>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
using namespace std;
void init();
#define REP(var, sum) for(sll var = 0; var < sum; var++)
typedef signed long long sll;
typedef unsigned long long ull;
/*
numeric_limits<sll>::max()
*/

int hs[100];
sll n;

int f(int l, int r) {
    if (l >= n || l > r)
        return 0;
    cout << "l=" << l << ", r=" << r << endl;
    int i;
    for(i = l; i <= r; i++) {
        if (!hs[i]) {
            break;
        }

        hs[i]--;
    }

    return f(l, i-1) + f(i+1, r) + 1;
}

int main() {
    init();
    cin >> n;
    REP(i, n) {
        cin >> hs[i];
    }
    cout << f(0, n-1) << endl;
    return 0;
}

void init() {
    cin.tie(0);
    ios::sync_with_stdio(false);
}