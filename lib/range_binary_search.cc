template<class ForwardIterator, class T>
ll range_binary_search(ForwardIterator first, ForwardIterator last, const T& needle) {
    ll left = 0, right = last - first;
    while (right - left > 1) {
        ll mid = (left + right) / 2;
        // printf(">>> (%lld, %lld) needle=%lld, mid=%lld (%lld)\n", left, right, needle, mid, first[mid]);
        if (needle < first[mid])
            right = mid;
        else
            left = mid;
    }

    return left;
}
