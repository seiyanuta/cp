ll gcd(ll a, ll b) {
    while (b != 0) {
        ll x = a % b;
        a = b;
        b = x;
    }

    return a;
}
