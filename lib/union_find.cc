template <typename T> class UnionFind {
    // Points to the parent if the value > 0  otherwise the # of nodes in
    // the cluster.
    vector<T> parent_of;

public:
    UnionFind(T num_nodes): parent_of(num_nodes) {
        REP(i, num_nodes) {
            parent_of[i] = -1;
        }
    }

    bool are_same_roots(T a, T b) {
        return root_of(a) == root_of(b);
    }

    T root_of(T a) {
        if (parent_of[a] < 0)
            return a;

        T root = root_of(parent_of[a]);
        parent_of[a] = root;
        return root;
    }

    void merge(T a, T b) {
        T ra = root_of(a);
        T rb = root_of(b);
        if (ra == rb)
            return;
        // Accumulate the # of nodes.
        parent_of[ra] += parent_of[rb];
        parent_of[rb] = ra;
    }

    size_t count(T a) {
        return -parent_of[root_of(a)];
    }
};