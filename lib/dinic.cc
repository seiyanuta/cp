class Edge {
public:
    ll src, dst, cap, rev;
    Edge(ll src, ll dst, ll cap, ll rev) : src(src), dst(dst), cap(cap), rev(rev) {}
};

class Dinic {
    ll num_nodes;
    vector<vector<Edge> > edges;
    vector<ll> distances;
    vector<ll> visited;

public:
    Dinic(ll num_nodes)
        : num_nodes(num_nodes)
        , edges(num_nodes)
        , distances(num_nodes, -1)
        , visited(num_nodes)
        {}

    void add_edge(ll src, ll dst, ll cap) {
        edges[src].emplace_back(src, dst, cap, edges[dst].size());
        edges[dst].emplace_back(dst, src, 0, edges[src].size() - 1);
    }

    ll max_flow(ll start, ll goal) {
        ll ans = 0;
        while (true) {
            bfs(start);
            if (distances[goal] < 0) {
                return ans;
            }

            visited.assign(num_nodes, 0);
            ll f;
            while ((f = dfs(start, goal, MAX_OF(ll))) > 0)
                ans += f;
        }
    }

private:
    // Computes the distances between `start` and each nodes whose
    // current capacity is not 0.
    void bfs(ll start) {
        distances.assign(num_nodes, -1);
        distances[start] = 0;
        queue<ll> qs;
        qs.push(start);
        while (!qs.empty()) {
            ll v = qs.front(); qs.pop();
            for (Edge& e: edges[v]) {
                if (e.cap > 0 && distances[e.dst] < 0) {
                    distances[e.dst] = distances[v] + 1;
                    qs.push(e.dst);
                }
            }
        }
    }

    ll dfs(ll src, ll dst, ll flow) {
        if (src == dst)
            return flow;

        for (ll& i = visited[src]; i < (ll) edges[src].size(); i++) {
            Edge& e = edges[src][i];
            if (e.cap > 0 && distances[src] < distances[e.dst]) {
                ll f = dfs(e.dst, dst, min(flow, e.cap));
                if (f > 0) {
                    e.cap -= f;
                    edges[e.dst][e.rev].cap += f;
                    return f;
                }
            }
        }

        return 0;
    }
};