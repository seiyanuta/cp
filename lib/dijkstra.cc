class Edge {
public:
    ll dst, cost;
    Edge(ll dst, ll cost) : dst(dst), cost(cost) {}
};

static const ll UNREACHABLE = MAX_OF(ll) / 10;
class Dijkstra {
    ll num_nodes;
    vector<vector<Edge> > edges;

public:
    Dijkstra(ll num_nodes) :
        num_nodes(num_nodes),
        edges(num_nodes, vector<Edge>()) {}

    void add_unidirect_edge(ll src, ll dst, ll cost) {
        edges[src].emplace_back(dst, cost);
    }

    void add_bidirect_edge(ll node1, ll node2, ll cost) {
        add_unidirect_edge(node1, node2, cost);
        add_unidirect_edge(node2, node1, cost);
    }

    // Returns the minimal costs (or `UNREACHABLE`) to each nodes and the route.
    pair<vector<ll>, vector<ll> > search(ll start) {
        vector<ll> costs(num_nodes, UNREACHABLE);
        vector<ll> prev(num_nodes, -1);
        priority_queue<pair<ll, ll> > qs;
        costs[start] = 0;
        qs.push(make_pair(0, start));
        while (!qs.empty()) {
            auto q = qs.top();
            qs.pop();
            ll node = q.second;
            for (auto& e: edges[node]) {
                if (costs[e.dst] > costs[node] + e.cost) {
                    costs[e.dst] = costs[node] + e.cost;
                    prev[e.dst] = node;
                    qs.push(make_pair(-costs[e.dst], e.dst));
                }
            }
        }

        return make_pair(costs, prev);
    }
};
